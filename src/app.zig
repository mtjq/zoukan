const std = @import("std");

const zk = @import("zoukan");
const core = zk.core;
const math = core.math;
const Vec3 = math.Vec3;
const Vec4 = math.Vec4;
const Quat = math.Quat;
const Mat4 = math.Mat4;
const ecs = zk.ecs;
const render = zk.render;
const camera = render.camera;
const Renderer = render.Renderer;
const Queries = render.Queries;
const mesh = render.mesh;


pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    const window = try core.window.init_window(allocator);
    defer core.window.deinit_window(allocator, window);

    var renderer = try Renderer.init(allocator, window); // Allocator not used.
    defer renderer.deinit();
    window.getUserPointer(core.window.WindowData).?.renderer = &renderer;

    renderer.setBgColour(
        render.Colour{.r = 0.1, .g = 0.1, .b = 0.12, .a = 1.0}
    );

    var input = core.input.Input.init(window);
    window.getUserPointer(core.window.WindowData).?.input = &input;

    const all_components = [_]type{
        mesh.Transform,
        mesh.BoxColourMesh,
    };

    var world = try ecs.World(&all_components).init(allocator);
    defer world.deinit();

    const ent1 = try world.newEntity();
    const transform1 = Mat4.translation(2.55, 1.95, -3.0)
        .mul(Mat4.rotation(0.2, .Z))
        .mul(Mat4.scale(1.2, 0.7, 3.0));
    try world.setComponent(ent1, mesh.Transform{
        .mat = transform1
    });
    try world.setComponent(ent1, mesh.BoxColourMesh{
        .colour = Vec4.new(0.2, 0.3, 0.7, 0.5),
        .roughness = 0.0,
        .reflectance = 0.5,
    });

    const ent2 = try world.newEntity();
    const transform2 = Mat4.translation(-0.95, -0.65, -0.45)
        .mul(Mat4.rotation(-0.4, .Z))
        .mul(Mat4.scale(0.7, 1.3, 1.0));
    try world.setComponent(ent2, mesh.Transform{
        .mat = transform2
    });
    try world.setComponent(ent2, mesh.BoxColourMesh{
        .colour = Vec4.new(0.8, 0.6, 0.1, 1.0),
        .roughness = 1.0,
        .reflectance = 0.5,
    });

    const proj = camera.PerspectiveProjection.new(0.5, 100.0, 0.4, 0.4);
    var cam = camera.Camera.new(
        Vec3.new(0.0, 0.0, 2.0),
        Quat.zero(),
        camera.Projection{.perspective = proj}
    );

    const lights = [2]render.light.DirectionalLight{
        .{
            .direction = Vec3.new(-1.0, -1.0, -1.0).normalise(),
            .colour = Vec3.new(0.9, 0.7, 0.95),
            .intensity = 40_000.0,
        },
        .{
            .direction = Vec3.new(0.5, 1.0, -0.5).normalise(),
            .colour = Vec3.new(0.95, 0.65, 0.85),
            .intensity = 50_000.0,
        },
    };

    var prev_time = std.time.microTimestamp();
    while (!window.shouldClose()) {
        const present_time = std.time.microTimestamp();
        const delta =
            @as(f32, @floatFromInt(present_time - prev_time)) / 1000000.0;
        prev_time = present_time;
        // Inputs.
        if (input.isKeyPressed(.y)) {
            window.setShouldClose(true);
        }
        if (input.isKeyPressed(.s)) {
            cam.translate(cam.right().mulScalar(- 8.0 * delta));
        }
        if (input.isKeyPressed(.f)) {
            cam.translate(cam.right().mulScalar(8.0 * delta));
        }
        if (input.isKeyPressed(.d)) {
            cam.translate(cam.up().mulScalar(- 8.0 * delta));
        }
        if (input.isKeyPressed(.e)) {
            cam.translate(cam.up().mulScalar(8.0 * delta));
        }
        if (input.cursorMoved()) {
            const qh = Quat.new(
                - 0.2 * input.cursor_movement.x() * delta,
                cam.up(),
                // Vec3.new(0.0, 1.0, 0.0),
            );
            const qv = Quat.new(
                - 0.2 * input.cursor_movement.y() * delta,
                cam.right(),
            );
            cam.rotate(qh.compose(qv));
        }
        if (input.scrolled()) {
            cam.translate(
                cam.forward().mulScalar(12.0 * delta * input.scroll_movement.y())
            );
        }

        // Render.
        var box_colour_query = (try world.query(
            &.{mesh.Transform, mesh.BoxColourMesh}
        )).?;
        defer box_colour_query.deinit();
        var queries = Queries{
            .box_colour = box_colour_query,
        };
        renderer.render(cam, &lights, &queries);

        // End frame.
        input.clean();
        core.window.pollEvents();
    }
}

