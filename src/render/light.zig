const mem = @import("std").mem;

const Vec3 =  @import("core").math.Vec3;


pub const DirectionalLight = extern struct {
    const Self = @This();

    direction: Vec3 align(16), // TODO: ensure it is normalised.
    colour: Vec3 align(16),
    intensity: f32, // Illuminance (lx or lm/m2).
    // Full moon: 1lx. Sky at noon: 25,000lx. Sun at noon: 105,000lx.

    pub inline fn slice(self: Self) []const f32 {
        return mem.bytesAsSlice(f32, mem.asBytes(&self));
    }
};
