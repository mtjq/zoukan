const std = @import("std");

const wgpu = @import("mach-gpu");

const Vec2 = @import("core").math.Vec2;
const Vec3 = @import("core").math.Vec3;
const Vec4 = @import("core").math.Vec4;
const Mat4 = @import("core").math.Mat4;


pub const BoxColourMesh = extern struct {
    const Self = @This();

    colour: Vec4,
    roughness: f32,
    reflectance: f32,

    pub inline fn slice(self: Self) []const f32 {
        return std.mem.bytesAsSlice(f32, std.mem.asBytes(&self));
    }
};

pub const Transform = extern struct {
    mat: Mat4,
};

pub const Vertex = extern struct {
    const Self = @This();

    pos: Vec3,
    normal: Vec3,

    // TODO: online works because inlined, dangling pointer.
    pub inline fn layout(
        comptime start_shader_location: u32,
    ) wgpu.VertexBufferLayout {
        const vertex_attributes = [2]wgpu.VertexAttribute{
            .{
                .format = .float32x3,
                .offset = @offsetOf(Vertex, "pos"),
                .shader_location = start_shader_location,
            },
            .{
                .format = .float32x3,
                .offset = @offsetOf(Vertex, "normal"),
                .shader_location = start_shader_location + 1,
            },
        };

        return wgpu.VertexBufferLayout{
            .array_stride = @sizeOf(Vertex),
            .step_mode = .vertex,
            .attribute_count = vertex_attributes.len,
            .attributes = &vertex_attributes,
        };
    }

    pub inline fn slice(self: Self) []const f32 {
        return std.mem.bytesAsSlice(f32, std.mem.asBytes(&self));
    }
};
