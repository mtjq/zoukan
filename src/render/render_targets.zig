const Size = @import("mach-glfw").Window.Size;
const wgpu = @import("mach-gpu");


pub const RenderTargets = struct {
    const Self = @This();

    depth_texture: *wgpu.Texture,
    depth_texture_view: *wgpu.TextureView,
    depth_stencil_attachment: wgpu.RenderPassDepthStencilAttachment,

    pub fn init(device: *wgpu.Device, frame_buffer_size: Size) Self {
        const format = wgpu.Texture.Format.depth24_plus;
        const depth_texture_desc = wgpu.Texture.Descriptor.init(.{
            .label = "Depth texture",
            .usage = .{.render_attachment = true},
            .dimension = .dimension_2d,
            .size = .{
                .width = frame_buffer_size.width,
                .height = frame_buffer_size.height,
                .depth_or_array_layers = 1
            },
            .format = format,
            .view_formats = (&format)[0..1],
        });
        const depth_texture = device.createTexture(&depth_texture_desc);

        const depth_texture_view_desc = wgpu.TextureView.Descriptor{
            .label = "Depth texture view",
            .format = format,
            .dimension = .dimension_2d,
            .base_mip_level = 0,
            .mip_level_count = 1,
            .base_array_layer = 0,
            .array_layer_count = 1,
            .aspect = .depth_only,
        };
        const depth_texture_view =
            depth_texture.createView(&depth_texture_view_desc);

        const depth_stencil_attachment = wgpu.RenderPassDepthStencilAttachment{
            .view = depth_texture_view,
            .depth_load_op = .clear,
            .depth_store_op = .store,
            .depth_clear_value = 1.0,
            .depth_read_only = .false,
            .stencil_load_op = .undefined,
            .stencil_store_op = .undefined,
            .stencil_clear_value = 0.0,
            .stencil_read_only = .true,
        };

        return Self{
            .depth_texture = depth_texture,
            .depth_texture_view = depth_texture_view,
            .depth_stencil_attachment = depth_stencil_attachment,
        };
    }

    pub fn deinit(self: *Self) void {
        self.depth_texture.destroy();
        self.depth_texture.release();
        self.depth_texture_view.release();
    }

    pub fn framebufferSizeCallback(
        self: *Self,
        device: *wgpu.Device,
        width: u32,
        height: u32,
    ) void {
        const format = self.depth_texture.getFormat();
        const depth_texture_desc = wgpu.Texture.Descriptor.init(.{
            .label = "Depth texture",
            .usage = self.depth_texture.getUsage(),
            .dimension = .dimension_2d,
            .size = .{
                .width = width,
                .height = height,
                .depth_or_array_layers =
                    self.depth_texture.getDepthOrArrayLayers(),
            },
            .format = format,
            .view_formats = (&format)[0..1],
        });
        const depth_texture =
            device.createTexture(&depth_texture_desc);

        const depth_texture_view_desc = wgpu.TextureView.Descriptor{
            .label = "Depth texture view",
            .format = format,
            .dimension = .dimension_2d,
            .base_mip_level = 0,
            .mip_level_count = self.depth_texture.getMipLevelCount(),
            .base_array_layer = 0,
            .array_layer_count = 1,
            .aspect = .depth_only,
        };
        const depth_texture_view =
            depth_texture.createView(&depth_texture_view_desc);

        self.depth_texture.destroy();
        self.depth_texture.release();
        self.depth_texture = depth_texture;

        self.depth_texture_view.release();
        self.depth_texture_view = depth_texture_view;

        self.depth_stencil_attachment.view = depth_texture_view;
    }
};
