const std = @import("std");
const wgpu = @import("mach-gpu");
const glfw = @import("mach-glfw");


pub const SEL = opaque {};
pub const Class = opaque {};

pub extern fn sel_getUid(str: [*c]const u8) ?*SEL;
pub extern fn objc_getClass(name: [*c]const u8) ?*Class;
pub extern fn objc_msgSend() void;

pub fn detectGlfwOptions() glfw.BackendOptions {
    const target = @import("builtin").target;
    if (target.isDarwin()) return .{ .cocoa = true };
    return switch (target.os.tag) {
        .windows => .{ .win32 = true },
        .linux => .{ .x11 = true, .wayland = true },
        else => .{},
    };
}

pub const RequestAdapterResponse = struct {
    status: wgpu.RequestAdapterStatus,
    adapter: ?*wgpu.Adapter,
    message: ?[*:0]const u8,
};

pub inline fn requestAdapterCallback(
    context: *RequestAdapterResponse,
    status: wgpu.RequestAdapterStatus,
    adapter: ?*wgpu.Adapter,
    message: ?[*:0]const u8,
) void {
    context.* = RequestAdapterResponse{
        .status = status,
        .adapter = adapter,
        .message = message,
    };
}

pub inline fn printUnhandledErrorCallback(
    _: void,
    typ: wgpu.ErrorType,
    message: [*:0]const u8
) void {
    switch (typ) {
        .validation => std.log.err("gpu: validation error: {s}\n", .{message}),
        .out_of_memory => std.log.err("gpu: out of memory: {s}\n", .{message}),
        .device_lost => std.log.err("gpu: device lost: {s}\n", .{message}),
        .unknown => std.log.err("gpu: unknown error: {s}\n", .{message}),
        else => unreachable,
    }
    std.process.exit(1);
}

pub fn createSurfaceForWindow(
    instance: *wgpu.Instance,
    window: glfw.Window,
    comptime glfw_options: glfw.BackendOptions,
) !*wgpu.Surface {
    const glfw_native = glfw.Native(glfw_options);
    if (glfw_options.win32) {
        return instance.createSurface(&wgpu.Surface.Descriptor{
            .next_in_chain = .{
                .from_windows_hwnd = &.{
                    .hinstance =
                        std.os.windows.kernel32.GetModuleHandleW(null).?,
                    .hwnd = glfw_native.getWin32Window(window),
                },
            },
        });
    } else if (glfw_options.x11) {
        return instance.createSurface(&wgpu.Surface.Descriptor{
            .next_in_chain = .{
                .from_xlib_window = &.{
                    .display = glfw_native.getX11Display(),
                    .window = glfw_native.getX11Window(window),
                },
            },
        });
    } else if (glfw_options.wayland) {
        return instance.createSurface(&wgpu.Surface.Descriptor{
            .next_in_chain = .{
                .from_wayland_surface = &.{
                    .display = glfw_native.getWaylandDisplay(),
                    .surface = glfw_native.getWaylandWindow(window),
                },
            },
        });
    } else if (glfw_options.cocoa) {
        const pool = try AutoReleasePool.init();
        defer AutoReleasePool.release(pool);

        const ns_window = glfw_native.getCocoaWindow(window);
        const ns_view = msgSend(ns_window, "contentView", .{}, *anyopaque);

        // Create a CAMetalLayer that covers the whole window
        // that will be passed to CreateSurface.
        msgSend(ns_view, "setWantsLayer:", .{true}, void);
        const layer = msgSend(
            objc_getClass("CAMetalLayer"),
            "layer",
            .{},
            ?*anyopaque
        );
        if (layer == null) @panic("failed to create Metal layer");
        msgSend(ns_view, "setLayer:", .{layer.?}, void);

        // Use retina if the window was created with retina support.
        const scale_factor = msgSend(ns_window, "backingScaleFactor", .{}, f64);
        msgSend(layer.?, "setContentsScale:", .{scale_factor}, void);

        return instance.createSurface(&wgpu.Surface.Descriptor{
            .next_in_chain = .{
                .from_metal_layer = &.{ .layer = layer.? },
            },
        });
    } else unreachable;
}

pub const AutoReleasePool = if (!@import("builtin").target.isDarwin()) opaque {
    pub fn init() error{OutOfMemory}!?*AutoReleasePool {
        return null;
    }

    pub fn release(pool: ?*AutoReleasePool) void {
        _ = pool;
        return;
    }
} else opaque {
    pub fn init() error{OutOfMemory}!?*AutoReleasePool {
        var pool = msgSend(
            objc_getClass("NSAutoreleasePool"),
            "alloc",
            .{},
            ?*AutoReleasePool
        );
        if (pool == null) return error.OutOfMemory;

        pool = msgSend(pool, "init", .{}, ?*AutoReleasePool);
        if (pool == null) unreachable;

        return pool;
    }

    pub fn release(pool: ?*AutoReleasePool) void {
        msgSend(pool, "release", .{}, void);
    }
};

// Borrowed from https://github.com/hazeycode/zig-objcrt
pub fn msgSend(
    obj: anytype,
    sel_name: [:0]const u8,
    args: anytype,
    comptime ReturnType: type
) ReturnType {
    const args_meta = @typeInfo(@TypeOf(args)).Struct.fields;

    const FnType = switch (args_meta.len) {
        0 => *const fn (
            @TypeOf(obj),
            ?*SEL,
        ) callconv(.C) ReturnType,
        1 => *const fn (
            @TypeOf(obj),
            ?*SEL,
            args_meta[0].type,
        ) callconv(.C) ReturnType,
        2 => *const fn (
            @TypeOf(obj),
            ?*SEL,
            args_meta[0].type,
            args_meta[1].type,
        ) callconv(.C) ReturnType,
        3 => *const fn (
            @TypeOf(obj),
            ?*SEL,
            args_meta[0].type,
            args_meta[1].type,
            args_meta[2].type,
        ) callconv(.C) ReturnType,
        4 => *const fn (
            @TypeOf(obj),
            ?*SEL,
            args_meta[0].type,
            args_meta[1].type,
            args_meta[2].type,
            args_meta[3].type,
        ) callconv(.C) ReturnType,
        else => @compileError("Unsupported number of args"),
    };

    const func = @as(FnType, @ptrCast(&objc_msgSend));
    const sel = sel_getUid(@as([*c]const u8, @ptrCast(sel_name)));

    return @call(.auto, func, .{ obj, sel } ++ args);
}
