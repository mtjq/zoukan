const PI = 3.141592;

struct VertexInput {
    @location(0) pos: vec3f,
    @location(1) normal: vec3f,
    @location(2) albedo: vec4f,
    @location(3) roughness: f32,
    @location(4) reflectance: f32,
};

struct ModelCols {
    @location(5) col0: vec4f,
    @location(6) col1: vec4f,
    @location(7) col2: vec4f,
    @location(8) col3: vec4f,
};

struct VertexOutput {
    @builtin(position) clip_pos: vec4f,
    @location(0) world_pos: vec3f,
    @location(1) normal: vec3f,
    @location(2) albedo: vec4f,
    @location(3) roughness: f32,
    @location(4) f0: f32,
};

struct Light {
    direction: vec3f,
    colour: vec3f,
    intensity: f32,
}

@group(0) @binding(0) var<uniform> view_proj_mat: mat4x4f;
@group(0) @binding(1) var<uniform> cam_pos: vec3f;
@group(0) @binding(2) var<uniform> lights: array<Light, 16>;


@vertex
fn vs_main(
    vertex: VertexInput,
    model_cols: ModelCols,
) -> VertexOutput {
    let model_mat = mat4x4f(
        model_cols.col0,
        model_cols.col1,
        model_cols.col2,
        model_cols.col3
    );

    let world_pos = model_mat * vec4f(vertex.pos, 1.0);

    var out: VertexOutput;
    out.clip_pos = view_proj_mat * world_pos;
    out.world_pos = world_pos.xyz;
    // Careful, only works if scale is uniform.
    // Also, we need to normalize in fragment shader because of the
    // interpolation, so we don't do it here. Also only works if scale is
    // uniform.
    out.normal = (model_mat * vec4f(vertex.normal, 0.0)).xyz;
    out.albedo = vertex.albedo;
    out.roughness = vertex.roughness;
    out.f0 = vertex.reflectance;

    return out;
}


@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4f {
    let n = normalize(in.normal.xyz);
    let v = normalize(cam_pos - in.world_pos.xyz);

    var luminance = vec3f(0.0);
    // TODO: max number of lights.
    for (var i: u32 = 0; i < 2; i++) {
        let l = - lights[i].direction;
        let NoL = max(dot(n, l), 0.0);
        if (NoL == 0.0) {
            continue; // TODO: until we have back-face culling for lighting.
        }

        let illuminance = lights[i].intensity * NoL * lights[i].colour;

        let h = normalize(v + l);
        let brdf_res =
            brdf(in.albedo.xyz, n, v, l, h, NoL, in.roughness, vec3f(in.f0));

        luminance += brdf_res * illuminance;
    }

    luminance /= 10000;

    return vec4f(luminance, 1.0);
}


fn brdf(
    albedo: vec3f,
    n: vec3f,
    v: vec3f,
    l: vec3f,
    h: vec3f,
    NoL: f32,
    perceptual_roughness: f32,
    linear_f0: vec3f,
) -> vec3f {
    let NoV = abs(dot(n, v)) + 1e-5;
    let NoH = max(dot(n, h), 0.0);
    let LoH = max(dot(l, h), 0.0);

    let roughness = max(perceptual_roughness * perceptual_roughness, 0.00785);
    let f0 = 0.16 * linear_f0 * linear_f0;

    let D = DGgx(NoH, roughness);
    let V = VSmithGgxCorrelated(NoV, NoL, roughness);
    let F = FSchlick(LoH, f0);

    let diffuse = albedo * lambert();
    let specular = (D * V) * F;

    return diffuse + specular;
}

fn DGgx(NoH: f32, a: f32) -> f32 {
    let a2 = a * a;
    let f = (NoH * a2 - NoH) * NoH + 1.0;
    return a2 / (PI * f * f);
}

fn VSmithGgxCorrelated(NoV: f32, NoL: f32, a: f32) -> f32 {
    let a2 = a * a;
    let ggx_v = NoL * sqrt(NoV * NoV * (1.0 - a2) + a2);
    let ggx_l = NoV * sqrt(NoL * NoL * (1.0 - a2) + a2);
    return 0.5 / (ggx_v + ggx_l);
}

// Less acurate, but removes both sqrt().
// fn VSmithGgxCorrelatedFast(NoV: f32, NoL: f32, roughness: f32) -> f32 {
//     f32 a2 = roughness;
//     f32 ggx_v = NoL * (NoV * (1.0 - a) + a);
//     f32 ggx_l = NoV * (NoL * (1.0 - a) + a);
//     return 0.5 / (ggx_v + ggx_l);
// }

// Can be more acurate.
fn FSchlick(LoH: f32, f0: vec3f) -> vec3f {
    let f = pow(1.0 - LoH, 5.0);
    return f + f0 * (1.0 - f);
}

fn lambert() -> f32 {
    return 1.0 / PI;
}
