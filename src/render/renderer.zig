const std = @import("std");
const glfw = @import("mach-glfw");
const Window = glfw.Window;
const wgpu = @import("mach-gpu");

const Query = @import("ecs").Query;

const setup_utils = @import("setup.zig");
const WindowData = @import("core").window.WindowData;
const RenderTargets = @import("render_targets.zig").RenderTargets;
const SubRenderers = @import("sub_renderers/mod.zig").SubRenderers;
const Queries = @import("sub_renderers/mod.zig").Queries;
const Camera = @import("camera.zig").Camera;
const DirectionalLight = @import("light.zig").DirectionalLight;

pub const Renderer = struct {
    allocator: std.mem.Allocator,
    window: glfw.Window,
    gpu: Gpu,
    bg_colour: wgpu.Color,
    render_targets: RenderTargets,
    sub_renderers: SubRenderers,

    const Self = @This();

    pub fn init(allocator: std.mem.Allocator, window: Window) !Self {
        try wgpu.Impl.init(allocator, .{});
        const gpu = try Gpu.init(allocator, window);

        // Depth stencil.
        const frame_buffer_size = window.getFramebufferSize();

        const render_targets =
            RenderTargets.init(gpu.device, frame_buffer_size);

        const depth_stencil_state = wgpu.DepthStencilState{
            .format = render_targets.depth_texture.getFormat(),
            .depth_write_enabled = .true,
            .depth_compare = .less,
            .stencil_read_mask = 0,
            .stencil_write_mask = 0,
        };

        return Self{
            .allocator = allocator,
            .window = window,
            .gpu = gpu,
            .bg_colour = wgpu.Color{.r = 0.0, .g = 0.0, .b = 0.0, .a = 1.0},
            .render_targets = render_targets,
            .sub_renderers = SubRenderers.init(
                window,
                gpu.device,
                depth_stencil_state
            ),
        };
    }

    pub fn deinit(self: *Self) void {
        self.sub_renderers.deinit();
        self.gpu.deinit();
        self.render_targets.deinit();
    }

    pub fn framebufferSizeCallback(self: *Self, width: u32, height: u32) void {
        self.render_targets.framebufferSizeCallback(
            self.gpu.device,
            width,
            height,
        );
    }

    pub fn setBgColour(self: *Self, colour: wgpu.Color) void {
        self.bg_colour = colour;
    }

    pub fn render(
        self: *Self,
        cam: Camera,
        lights: []const DirectionalLight,
        queries: *Queries,
    ) void {
        // Setup.
        const wd = self.window.getUserPointer(WindowData).?;
        if (
            wd.swap_chain == null
            or !std.meta.eql(wd.current_desc, wd.target_desc)
        ) {
            wd.swap_chain = self.gpu.device.createSwapChain(
                wd.surface,
                &wd.target_desc
            );
            wd.current_desc = wd.target_desc;
        }

        const background_colour = self.bg_colour;
        const back_buffer_view = wd.swap_chain.?.getCurrentTextureView().?;
        defer back_buffer_view.release();
        const colour_attachment = wgpu.RenderPassColorAttachment{
            .view = back_buffer_view,
            .resolve_target = null,
            .clear_value = background_colour,
            .load_op = .clear,
            .store_op = .store,
        };

        self.sub_renderers.render(
            self.gpu,
            colour_attachment,
            self.render_targets.depth_stencil_attachment,
            cam,
            lights,
            queries
        );

        // Finish.
        wd.swap_chain.?.present();
        self.gpu.device.tick();
    }

};

pub const Gpu = struct {
    instance: *wgpu.Instance,
    adapter: *wgpu.Adapter,
    surface: *wgpu.Surface,
    device: *wgpu.Device,
    queue: *wgpu.Queue,

    fn init(allocator: std.mem.Allocator, window: glfw.Window) !Gpu {
        const instance = wgpu.createInstance(null) orelse {
            std.log.err("failed to create GPU instance", .{});
            std.process.exit(1);
        };

        const surface = try setup_utils.createSurfaceForWindow(
            instance,
            window,
            comptime setup_utils.detectGlfwOptions());

        var response: setup_utils.RequestAdapterResponse = undefined;
        instance.requestAdapter(
            &wgpu.RequestAdapterOptions{
                .compatible_surface = surface,
                .power_preference = .high_performance,
                .force_fallback_adapter = .false,
            },
            &response,
            setup_utils.requestAdapterCallback
        );
        if (response.status != .success) {
            std.log.err(
                "failed to create GPU adapter: {s}",
                .{response.message.?}
            );
            std.process.exit(1);
        }
        const adapter = response.adapter.?;

        var adapter_props = std.mem.zeroes(wgpu.Adapter.Properties);
        adapter.getProperties(&adapter_props);
        std.log.info("found {s} backend on {s} adapter: {s}, {s}", .{
            adapter_props.backend_type.name(),
            adapter_props.adapter_type.name(),
            adapter_props.name,
            adapter_props.driver_description,
        });

        // TODO: use with a LostCallback or when init() is repaired.
        // const required_features = [_]gpu.FeatureName{
        //     gpu.FeatureName.undefined,
        // };
        // const required_limits = gpu.RequiredLimits{
        //     .limits = gpu.Limits{
        //         // .max_buffer_size = 4 * @sizeOf(Vertex),
        //     },
        // };
        // const device_descriptor = gpu.Device.Descriptor.init(.{
        //     .label = "test device descriptor",
        //     .required_features = &required_features,
        //     .required_limits = &required_limits,
        // });
        // const device = adapter.createDevice(&device_descriptor) orelse {
        //     std.debug.print("failed to create GPU device\n", .{});
        //     std.process.exit(1);
        // };
        const device = adapter.createDevice(null) orelse {
            std.debug.print("failed to create GPU device\n", .{});
            std.process.exit(1);
        };
        device.setUncapturedErrorCallback(
            {},
            setup_utils.printUnhandledErrorCallback
        );

        const framebuffer_size = window.getFramebufferSize();
        // TODO: should not be here?
        const window_data = try allocator.create(WindowData);
        window_data.* = .{
            .surface = surface,
            .swap_chain = null,
            .swap_chain_format = undefined,
            .current_desc = undefined,
            .target_desc = undefined,
            .renderer = null,
            .input = null,
        };
        window.setUserPointer(window_data);

        // TODO: check if other swap chain formats are released.
        window_data.swap_chain_format = .bgra8_unorm;
        const swap_chain_descriptor = wgpu.SwapChain.Descriptor{
            .label = "basic swap chain",
            .usage = .{ .render_attachment = true },
            .format = window_data.swap_chain_format,
            .width = framebuffer_size.width,
            .height = framebuffer_size.height,
            .present_mode = .fifo,
        };

        window_data.current_desc = swap_chain_descriptor;
        window_data.target_desc = swap_chain_descriptor;

        return Gpu{
            .instance = instance,
            .adapter = adapter,
            .surface = surface,
            .device = device,
            .queue = device.getQueue(),
        };
    }

    fn deinit(self: *Gpu) void {
        self.queue.release();
        self.device.release();
        self.surface.release();
        self.adapter.release();
        self.instance.release();
    }
};

