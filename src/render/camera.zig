const Vec3 = @import("core").math.Vec3;
const Quat = @import("core").math.Quat;
const Mat4 = @import("core").math.Mat4;


pub const Camera = struct {
    const Self = @This();

    pos: Vec3,
    rot: Quat,
    projection: Projection,

    pub fn new(p: Vec3, r: Quat, proj: Projection) Self {
        return Self{
            .pos = p,
            .rot = r,
            .projection = proj
        };
    }

    pub fn translate(self: *Self, t: Vec3) void {
        self.pos = self.pos.add(t);
    }

    pub fn rotate(self: *Self, r: Quat) void {
        self.rot = r.compose(self.rot);
    }

    pub fn right(self: Self) Vec3 {
        return self.rot.apply(Vec3.new(1.0, 0.0, 0.0));
    }

    pub fn up(self: Self) Vec3 {
        return self.rot.apply(Vec3.new(0.0, 1.0, 0.0));
    }

    pub fn forward(self: Self) Vec3 {
        return self.rot.apply(Vec3.new(0.0, 0.0, - 1.0));
    }

    pub fn getTransform(self: Self) Mat4 {
        return Mat4.fromTransQuat(self.pos, self.rot);
    }

    pub fn getInvTransform(self: Self) Mat4 {
        return self.getTransform().inverseTransform();
    }

    pub fn getView(self: Self) Mat4 {
        return self.getInvTransform();
    }

    pub fn getProj(self: Self) Mat4 {
        return self.projection.mat();
    }

    pub fn getViewProj(self: Self) Mat4 {
        return self.projection.mat().mul(self.getInvTransform());
    }
};

pub const Projection = union(enum) {
    const Self = @This();

    ortho: OrthographicProjection,
    perspective: PerspectiveProjection,

    pub fn mat(self: Self) Mat4 {
        return switch(self) {
            .ortho => |proj| proj.mat,
            .perspective => |proj| proj.mat,
        };
    }
};

// TODO: non symmetrical.
pub const PerspectiveProjection = struct {
    const Self = @This();

    mat: Mat4,
    near: f32,
    far: f32,
    left: f32,
    right: f32,
    bottom: f32,
    top: f32,

    pub fn new(near: f32, far: f32, lr: f32, bt: f32) Self {
        var res = Mat4.scale(
            near / lr,
            near / bt,
            - far / (far - near)
        );
        res.set(3, 3, 0.0);
        res.set(2, 3, - far * near / (far - near));
        res.set(3, 2, -1.0);

        return Self{
            .mat = res,
            .near = near,
            .far = far,
            .left = lr,
            .right = lr,
            .bottom = bt,
            .top = bt,
        };
    }
};

// TODO: non symmetrical.
pub const OrthographicProjection = struct {
    const Self = @This();

    mat: Mat4,
    near: f32,
    far: f32,
    left: f32,
    right: f32,
    bottom: f32,
    top: f32,

    pub fn new(near: f32, far: f32, lr: f32, bt: f32) Self {
        return Self{
            .mat = Mat4.scale(1.0 / lr, 1.0 / bt, - 2.0 / (far - near)),
            .near = near,
            .far = far,
            .left = lr,
            .right = lr,
            .bottom = bt,
            .top = bt,
        };
    }
};

