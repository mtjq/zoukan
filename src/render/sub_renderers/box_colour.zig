const std = @import("std");
const Window = @import("mach-glfw").Window;
const wgpu = @import("mach-gpu");

const WindowData = @import("core").window.WindowData;
const Vec3 = @import("core").math.Vec3;
const Vec4 = @import("core").math.Vec4;
const Mat4 = @import("core").math.Mat4;
const Query = @import("ecs").Query;

const Gpu = @import("../renderer.zig").Gpu;
const mesh = @import("../mesh.zig");
const Transform = mesh.Transform;
const BoxColourMesh = mesh.BoxColourMesh;
const Vertex = mesh.Vertex;
const Camera = @import("../camera.zig").Camera;
const DirectionalLight = @import("../light.zig").DirectionalLight;


const max_lights = 16; // TODO: max number.

pub const BoxColourRenderer = struct {
    pub const QueryType = Query(&.{Transform, BoxColourMesh});

    pipeline: *wgpu.RenderPipeline,
    vertex_buff: *wgpu.Buffer,
    index_buff: *wgpu.Buffer,
    lighting_buff: *wgpu.Buffer,
    model_mat_buff: *wgpu.Buffer,
    inv_trans_model_mat_buff: *wgpu.Buffer,
    view_proj_mat_buff: *wgpu.Buffer,
    cam_pos_buff: *wgpu.Buffer,
    light_buff: *wgpu.Buffer,
    bind_group: *wgpu.BindGroup,

    const Self = @This();

    pub fn init(
        window: Window,
        device: *wgpu.Device,
        depth_stencil_state: wgpu.DepthStencilState,
    ) Self {
        // Vertex buffer.
        const normal_front = Vec3.new(0.0, 0.0, 1.0);
        const normal_top = Vec3.new(0.0, 1.0, 0.0);
        const normal_right = Vec3.new(1.0, 0.0, 0.0);
        const v0 = Vec3.new(-0.5, -0.5,  0.5);
        const v1 = Vec3.new( 0.5, -0.5,  0.5);
        const v2 = Vec3.new( 0.5,  0.5,  0.5);
        const v3 = Vec3.new(-0.5,  0.5,  0.5);
        const v4 = Vec3.new(-0.5, -0.5, -0.5);
        const v5 = Vec3.new( 0.5, -0.5, -0.5);
        const v6 = Vec3.new( 0.5,  0.5, -0.5);
        const v7 = Vec3.new(-0.5,  0.5, -0.5);
        const to_vertex_buff = [_]Vertex{
            // Front.
            .{.pos = v0, .normal = normal_front},
            .{.pos = v1, .normal = normal_front},
            .{.pos = v2, .normal = normal_front},
            .{.pos = v3, .normal = normal_front},
            // Bottom.
            .{.pos = v0, .normal = normal_top.neg()},
            .{.pos = v4, .normal = normal_top.neg()},
            .{.pos = v5, .normal = normal_top.neg()},
            .{.pos = v1, .normal = normal_top.neg()},
            // Right.
            .{.pos = v1, .normal = normal_right},
            .{.pos = v5, .normal = normal_right},
            .{.pos = v6, .normal = normal_right},
            .{.pos = v2, .normal = normal_right},
            // Top.
            .{.pos = v3, .normal = normal_top},
            .{.pos = v2, .normal = normal_top},
            .{.pos = v6, .normal = normal_top},
            .{.pos = v7, .normal = normal_top},
            // Left.
            .{.pos = v4, .normal = normal_right.neg()},
            .{.pos = v0, .normal = normal_right.neg()},
            .{.pos = v3, .normal = normal_right.neg()},
            .{.pos = v7, .normal = normal_right.neg()},
            // Back.
            .{.pos = v5, .normal = normal_front.neg()},
            .{.pos = v4, .normal = normal_front.neg()},
            .{.pos = v7, .normal = normal_front.neg()},
            .{.pos = v6, .normal = normal_front.neg()},
        };
        const vertex_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour vertex buffer",
                .usage = .{.vertex = true},
                .size = @sizeOf(@TypeOf(to_vertex_buff)),
                .mapped_at_creation = .true,
            }
        );
        const mapped_vertex_buff = vertex_buff.getMappedRange(
            Vertex, 0, to_vertex_buff.len
        ).?;
        @memcpy(mapped_vertex_buff, &to_vertex_buff);
        vertex_buff.unmap();

        // Index buffer.
        const to_index_buff = [_]u32{
            // Front.
            0, 1, 2,
            2, 3, 0,
            // Bottom.
            4, 5, 6,
            6, 7, 4,
            // Right.
            8, 9, 10,
            10, 11, 8,
            // Top.
            12, 13, 14,
            14, 15, 12,
            // Left.
            16, 17, 18,
            18, 19, 16,
            // Back.
            20, 21, 22,
            22, 23, 20,
        };
        const index_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour index buffer",
                .usage = .{.index = true},
                .size = @sizeOf(@TypeOf(to_index_buff)),
                .mapped_at_creation = .true,
            }
        );
        const mapped_index_buff = index_buff.getMappedRange(
            u32, 0, to_index_buff.len
        ).?;
        @memcpy(mapped_index_buff, &to_index_buff);
        index_buff.unmap();

        // Lighting buffer.
        const lighting_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour lighting buffer",
                .usage = .{.vertex = true, .copy_dst = true},
                .size = 128 * @sizeOf(BoxColourMesh),
                .mapped_at_creation = .false,
            }
        );
        const lighting_layout = wgpu.VertexBufferLayout{
            .array_stride = @sizeOf(BoxColourMesh),
            .step_mode = .instance,
            .attribute_count = 3,
            .attributes = &[3]wgpu.VertexAttribute{
                .{
                    .format = .float32x4,
                    .offset = 0,
                    .shader_location = 2,
                },
                .{
                    .format = .float32,
                    .offset = @offsetOf(BoxColourMesh, "roughness"),
                    .shader_location = 3,
                },
                .{
                    .format = .float32,
                    .offset = @offsetOf(BoxColourMesh, "reflectance"),
                    .shader_location = 4,
                },
            },
        };

        // Model mat buffer.
        const model_mat_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour model mat buffer",
                .usage = .{.vertex = true, .copy_dst = true},
                .size = 128 * @sizeOf(Mat4),
                .mapped_at_creation = .false,
            }
        );

        // Model mat buffer.
        const inv_trans_model_mat_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour inv trans model mat buffer",
                .usage = .{.vertex = true, .copy_dst = true},
                .size = 128 * @sizeOf(Mat4),
                .mapped_at_creation = .false,
            }
        );

        // View proj mat buffer.
        const view_proj_mat_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour view proj mat buffer",
                .usage = .{.uniform = true, .copy_dst = true},
                .size = @sizeOf(Mat4),
                .mapped_at_creation = .false,
            }
        );
        const view_proj_mat_layout_entry = wgpu.BindGroupLayout.Entry.buffer(
            0,
            wgpu.ShaderStageFlags{.vertex = true},
            wgpu.Buffer.BindingType.uniform,
            false,
            @sizeOf(Mat4),
        );
        const view_proj_mat_entry = wgpu.BindGroup.Entry.buffer(
            0,
            view_proj_mat_buff,
            0,
            @sizeOf(Mat4),
        );

        // Cam pos buffer.
        const cam_pos_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour cam pos buffer",
                .usage = .{.uniform = true, .copy_dst = true},
                .size = @sizeOf(Vec3),
                .mapped_at_creation = .false,
            }
        );
        const cam_pos_layout_entry = wgpu.BindGroupLayout.Entry.buffer(
            1,
            wgpu.ShaderStageFlags{.fragment = true},
            wgpu.Buffer.BindingType.uniform,
            false,
            @sizeOf(Vec3),
        );
        const cam_pos_entry = wgpu.BindGroup.Entry.buffer(
            1,
            cam_pos_buff,
            0,
            @sizeOf(Vec3),
        );

        // Light buffer.
        const light_buff = device.createBuffer(
            &wgpu.Buffer.Descriptor{
                .label = "BoxColour light buffer",
                .usage = .{.uniform = true, .copy_dst = true},
                .size = max_lights * @sizeOf(DirectionalLight),
                .mapped_at_creation = .false,
            }
        );
        const light_layout_entry = wgpu.BindGroupLayout.Entry.buffer(
            2,
            wgpu.ShaderStageFlags{.fragment = true},
            wgpu.Buffer.BindingType.uniform,
            false,
            max_lights * @sizeOf(DirectionalLight),
        );
        const light_entry = wgpu.BindGroup.Entry.buffer(
            2,
            light_buff,
            0,
            max_lights * @sizeOf(DirectionalLight),
        );

        // Bind group layout.
        const bind_group_layout_descriptor =
            wgpu.BindGroupLayout.Descriptor.init(.{
                .label = "BoxColour bind group layout",
                .entries = &.{
                    view_proj_mat_layout_entry,
                    cam_pos_layout_entry,
                    light_layout_entry,
                },
        });
        const bind_group_layout = device.createBindGroupLayout(
            &bind_group_layout_descriptor
        );
        defer bind_group_layout.release();
        // Bind group.
        const bind_group_descriptor = wgpu.BindGroup.Descriptor.init(.{
            .label = "BoxColour bind group",
            .layout = bind_group_layout,
            .entries = &.{
                view_proj_mat_entry,
                cam_pos_entry,
                light_entry,
            },
        });
        const bind_group = device.createBindGroup(&bind_group_descriptor);

        // Shader.
        const shader_module = device.createShaderModuleWGSL(
            "BoxColour Shader",
            @embedFile("../shaders/box_colour.wgsl")
        );
        defer shader_module.release();

        // Color targer.
        const blend = wgpu.BlendState{};
        const colour_target = wgpu.ColorTargetState{
            .format = window
                .getUserPointer(WindowData).?
                .swap_chain_format,
            .blend = &blend,
            .write_mask = wgpu.ColorWriteMaskFlags.all,
        };

        // Pipeline layout.
        const pipeline_layout_descriptor =
            wgpu.PipelineLayout.Descriptor.init(.{
                .label = "pipeline layout",
                .bind_group_layouts = &.{bind_group_layout},
        });
        const pipeline_layout = device.createPipelineLayout(
            &pipeline_layout_descriptor
        );
        defer pipeline_layout.release();

        // Pipeline.
        const pipeline_descriptor = wgpu.RenderPipeline.Descriptor{
            .layout = pipeline_layout,
            .vertex = wgpu.VertexState.init(.{
                .module = shader_module,
                .entry_point = "vs_main",
                .buffers = &.{
                    Vertex.layout(0),
                    lighting_layout,
                    Mat4.layout(5, .instance),
                    Mat4.layout(9, .instance),
                }
            }),
            .fragment = &wgpu.FragmentState.init(.{
                .module = shader_module,
                .entry_point = "fs_main",
                .targets = &.{colour_target},
            }),
            .depth_stencil = &depth_stencil_state,
            .multisample = .{},
            .primitive = .{},
        };
        const pipeline = device.createRenderPipeline(&pipeline_descriptor);

        return Self {
            .pipeline = pipeline,
            .vertex_buff = vertex_buff,
            .index_buff = index_buff,
            .lighting_buff = lighting_buff,
            .model_mat_buff = model_mat_buff,
            .inv_trans_model_mat_buff = inv_trans_model_mat_buff,
            .view_proj_mat_buff = view_proj_mat_buff,
            .cam_pos_buff = cam_pos_buff,
            .light_buff = light_buff,
            .bind_group = bind_group,
        };
    }

    pub fn deinit(self: *Self) void {
        self.pipeline.release();
        self.vertex_buff.release();
        self.index_buff.release();
        self.lighting_buff.release();
        self.model_mat_buff.release();
        self.inv_trans_model_mat_buff.release();
        self.view_proj_mat_buff.release();
        self.light_buff.release();
        self.bind_group.release();
    }

    pub fn render(
        self: *Self,
        gpu: Gpu,
        colour_attachment: wgpu.RenderPassColorAttachment,
        depth_stencil_attachment: wgpu.RenderPassDepthStencilAttachment,
        cam: Camera,
        lights: []const DirectionalLight,
        query: *QueryType,
    ) void {
        const encoder = gpu.device.createCommandEncoder(null);

        const render_pass_descriptor = wgpu.RenderPassDescriptor.init(.{
            .label = "Box colour render pass",
            .color_attachments = &.{colour_attachment},
            .depth_stencil_attachment = &depth_stencil_attachment,
            .occlusion_query_set = null,
            .timestamp_writes = null,
        });
        const pass = encoder.beginRenderPass(&render_pass_descriptor);
        pass.setPipeline(self.pipeline);
        pass.setVertexBuffer(
            0, self.vertex_buff,
            0, self.vertex_buff.getSize()
        );
        pass.setVertexBuffer(
            1, self.lighting_buff,
            0, self.lighting_buff.getSize()
        );
        pass.setVertexBuffer(
            2, self.model_mat_buff,
            0, self.model_mat_buff.getSize()
        );
        pass.setVertexBuffer(
            3, self.inv_trans_model_mat_buff,
            0, self.inv_trans_model_mat_buff.getSize()
        );
        var draw_count: u64 = 0;
        while (query.next()) |q| {
            const model_mat = q.Transform.mat;

            gpu.queue.writeBuffer(
                self.lighting_buff,
                draw_count * @sizeOf(BoxColourMesh),
                q.BoxColourMesh.slice()
            );
            gpu.queue.writeBuffer(
                self.model_mat_buff,
                draw_count * @sizeOf(Mat4),
                model_mat.slice()
            );
            gpu.queue.writeBuffer(
                self.inv_trans_model_mat_buff,
                draw_count * @sizeOf(Mat4),
                model_mat.inverseTransform().transpose().slice()
            );

            draw_count += 1;
        }
        pass.setIndexBuffer(
            self.index_buff,
            .uint32,
            0, self.index_buff.getSize()
        );
        pass.setBindGroup(0, self.bind_group, null);
        gpu.queue.writeBuffer(
            self.view_proj_mat_buff,
            0, cam.getViewProj().slice()
        );
        gpu.queue.writeBuffer(
            self.cam_pos_buff,
            0, cam.pos.slice()
        );
        for (0..@min(lights.len, max_lights)) |i| {
            gpu.queue.writeBuffer(
                self.light_buff,
                i * @sizeOf(DirectionalLight),
                lights[i].slice()
            );
        }

        pass.drawIndexed(36, @truncate(draw_count), 0, 0, 0);
        pass.end();
        pass.release();

        var command = encoder.finish(null);
        encoder.release();

        gpu.queue.submit(&[_]*wgpu.CommandBuffer{command});
        command.release();
    }
};
