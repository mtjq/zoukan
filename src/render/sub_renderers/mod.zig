const StructField = @import("std").builtin.Type.StructField;
const Window = @import("mach-glfw").Window;
const wgpu = @import("mach-gpu");

const Gpu = @import("../renderer.zig").Gpu;
const BoxColourRenderer = @import("box_colour.zig").BoxColourRenderer;
const Camera = @import("../camera.zig").Camera;
const DirectionalLight = @import("../light.zig").DirectionalLight;


pub const SubRenderers = struct {
    const Self = @This();

    box_colour: BoxColourRenderer,

    pub fn init(
        window: Window,
        device: *wgpu.Device,
        depth_stencil_state: wgpu.DepthStencilState,
    ) Self {
        return Self{
            .box_colour = BoxColourRenderer.init(
                window,
                device,
                depth_stencil_state
            ),
        };
    }

    pub fn deinit(self: *Self) void {
        self.box_colour.deinit();
    }

    pub fn render(
        self: *Self,
        gpu: Gpu,
        colour_attachment: wgpu.RenderPassColorAttachment,
        depth_stencil_attachment: wgpu.RenderPassDepthStencilAttachment,
        cam: Camera,
        lights: []const DirectionalLight,
        queries: *Queries,
    ) void {
        self.box_colour.render(
            gpu,
            colour_attachment,
            depth_stencil_attachment,
            cam,
            lights,
            &queries.box_colour
        );
    }
};

pub const Queries = Q: {
    const sub_rend_fields = @typeInfo(SubRenderers).Struct.fields;
    const len = sub_rend_fields.len;
    var fields: [len]StructField = undefined;
    for (0..len) |i| {
        const T = sub_rend_fields[i].type.QueryType;
        fields[i] = .{
            .name = sub_rend_fields[i].name,
            .type = T,
            .default_value = null,
            .is_comptime = false,
            .alignment = @alignOf(T),
        };
    }
    break :Q @Type(.{.Struct = .{
        .layout = .auto,
        .backing_integer = null,
        .fields = &fields,
        .decls = &.{},
        .is_tuple = false,
    }});
};
