const std = @import("std");
const glfw = @import("mach-glfw");
const Window = glfw.Window;
const wgpu = @import("mach-gpu");

const Input = @import("input.zig").Input;
const Vec2 = @import("math.zig").Vec2;
const Renderer = @import("render").Renderer;


pub const WindowData = struct {
    surface: *wgpu.Surface,
    swap_chain: ?*wgpu.SwapChain,
    swap_chain_format: wgpu.Texture.Format,
    current_desc: wgpu.SwapChain.Descriptor,
    target_desc: wgpu.SwapChain.Descriptor,
    renderer: ?*Renderer,
    input: ?*Input,
};

pub fn init_window(allocator: std.mem.Allocator) !Window {
    const backend_type = try detectBackendType(allocator);

    glfw.setErrorCallback(errorCallback);

    if (!glfw.init(.{})) {
        std.log.err(
            "failed to initialize GLFW: {?s}",
            .{glfw.getErrorString()}
        );
        std.process.exit(1);
    }

    var hints = glfwWindowHintsForBackend(backend_type);
    hints.cocoa_retina_framebuffer = true;

    const window = glfw.Window.create(
        1920, 1080,
        "Zoukan",
        null, null,
        hints
    ) orelse {
        std.log.err(
            "failed to create GLFW window: {?s}",
            .{glfw.getErrorString()}
        );
        std.process.exit(1);
    };

    window.setInputModeCursor(.disabled);

    window.setFramebufferSizeCallback((struct {
        fn callback(w: Window, width: u32, height: u32) void {
            const wd = w.getUserPointer(WindowData).?;
            wd.target_desc.width = width;
            wd.target_desc.height = height;

            if (wd.renderer) |r| {
                r.framebufferSizeCallback(width, height);
            }
        }
    }).callback);

    window.setCursorPosCallback((struct {
        fn callback(w: Window, xpos: f64, ypos: f64) void {
            const input = w.getUserPointer(WindowData).?.input.?;
            const x: f32 = @floatCast(xpos);
            const y: f32 = @floatCast(ypos);
            input.cursor_movement = Vec2.new(
                x - input.cursor_pos.x(),
                y - input.cursor_pos.y(),
            );
            input.cursor_pos = Vec2.new(x, y);
        }
    }).callback);

    window.setScrollCallback((struct {
        fn callback(w: Window, xoffset: f64, yoffset: f64) void {
            const input = w.getUserPointer(WindowData).?.input.?;
            const x: f32 = @floatCast(xoffset);
            const y: f32 = @floatCast(yoffset);
            input.scroll_movement = Vec2.new(x, y);
        }
    }).callback);

    if (backend_type == .opengl or backend_type == .opengles) {
        glfw.makeContextCurrent(window);
    }

    return window;
}

pub fn deinit_window(allocator: std.mem.Allocator, window: glfw.Window) void {
    allocator.destroy(window.getUserPointer(WindowData).?);
    window.destroy();
    glfw.terminate();
}

pub fn pollEvents() void {
    glfw.pollEvents();
}

fn detectBackendType(allocator: std.mem.Allocator) !wgpu.BackendType {
    const gpu_backend = try getEnvVarOwned(allocator, "MACH_GPU_BACKEND");
    if (gpu_backend) |backend| {
        defer allocator.free(backend);
        if (std.ascii.eqlIgnoreCase(backend, "null")) return .null;
        if (std.ascii.eqlIgnoreCase(backend, "d3d11")) return .d3d11;
        if (std.ascii.eqlIgnoreCase(backend, "d3d12")) return .d3d12;
        if (std.ascii.eqlIgnoreCase(backend, "metal")) return .metal;
        if (std.ascii.eqlIgnoreCase(backend, "vulkan")) return .vulkan;
        if (std.ascii.eqlIgnoreCase(backend, "opengl")) return .opengl;
        if (std.ascii.eqlIgnoreCase(backend, "opengles")) return .opengles;
        @panic("unknown MACH_GPU_BACKEND type");
    }

    const target = @import("builtin").target;
    if (target.isDarwin()) return .metal;
    if (target.os.tag == .windows) return .d3d12;
    return .vulkan;
}

fn getEnvVarOwned(allocator: std.mem.Allocator, key: []const u8)
error{OutOfMemory, InvalidUtf8, InvalidWtf8}!?[]u8 {
    return std.process.getEnvVarOwned(allocator, key) catch |err| switch (err) {
        error.EnvironmentVariableNotFound => @as(?[]u8, null),
        else => |e| e,
    };
}

fn errorCallback(error_code: glfw.ErrorCode, description: [:0]const u8) void {
    std.log.err("glfw: {}: {s}\n", .{ error_code, description });
}

fn glfwWindowHintsForBackend(backend: wgpu.BackendType) glfw.Window.Hints {
    return switch (backend) {
        .opengl => .{
            .context_version_major = 4,
            .context_version_minor = 4,
            .opengl_forward_compat = true,
            .opengl_profile = .opengl_core_profile,
        },
        .opengles => .{
            .context_version_major = 3,
            .context_version_minor = 1,
            .client_api = .opengl_es_api,
            .context_creation_api = .egl_context_api,
        },
        else => .{
            // Without this GLFW will initialize a GL context on the window,
            // which prevents using the window with other APIs.
            .client_api = .no_api,
        },
    };
}

