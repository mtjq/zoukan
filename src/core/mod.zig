pub const utils = @import("utils.zig");
pub const math = @import("math.zig");
pub const window = @import("window.zig");
pub const input = @import("input.zig");


test {
    _ = utils;
    _ = math;
    _ = input;
}
