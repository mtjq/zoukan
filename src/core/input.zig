const glfw = @import("mach-glfw");
const Window = glfw.Window;
const Key = glfw.Key;

const Vec2 = @import("math.zig").Vec2;


pub const Input = struct {
    const Self = @This();

    window: Window,
    cursor_pos: Vec2,
    cursor_movement: Vec2,
    scroll_movement: Vec2,

    pub fn init(window: Window) Self {
        return Self{
            .window = window,
            .cursor_pos = Vec2.zero(),
            .cursor_movement = Vec2.zero(),
            .scroll_movement = Vec2.zero(),
        };
    }

    pub fn clean(self: *Self) void {
        self.cursor_movement = Vec2.zero();
        self.scroll_movement = Vec2.zero();
    }

    pub fn isKeyPressed(self: Self, key: Key) bool {
        return (self.window.getKey(key) == .press);
    }

    pub fn isKeyReleased(self: Self, key: Key) bool {
        return (self.window.getKey(key) == .release);
    }

    pub fn cursorMoved(self: Self) bool {
        return !(self.cursor_movement.eql(Vec2.zero()));
    }

    pub fn scrolled(self: Self) bool {
        return !(self.scroll_movement.eql(Vec2.zero()));
    }
};
