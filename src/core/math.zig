const std = @import("std");
const mem = std.mem;

const wgpu = @import("mach-gpu");


pub const Vec2 = Vec(2, f32);
pub const Vec3 = Vec(3, f32);
pub const Vec4 = Vec(4, f32);

pub const Mat2 = Mat(2, f32);
pub const Mat3 = Mat(3, f32);
pub const Mat4 = Mat(4, f32);

pub fn Vec(comptime dim: usize, comptime T: type) type {
    return extern struct {
        const Self = @This();
        const V = [dim]T;
        const Simd = @Vector(dim, T);

        pub const gpu_align = switch(dim) {
            2 => 8,
            3 => 16,
            4 => 16,
            else => @compileError(
                "Cannot create a Vector of dimension other than 2, 3 or 4."
            ),
        };

        v: V ,

        const zero_val: T = switch(@typeInfo(T)) {
            .Float => 0.0,
            .Int => 0,
            else => @compileError(
                "Cannot create a Vector of type other than float or int."
            ),
        };
        const one_val: T = switch(@typeInfo(T)) {
            .Float => 1.0,
            .Int => 1,
            else => @compileError(
                "Cannot create a Vector of type other than float or int."
            ),
        };

        pub usingnamespace switch(@typeInfo(T)) {
            .Float => struct {
                pub fn normalise(self: Self) Self {
                    return self.mulScalar(1.0 / self.norm());
                }

                pub inline fn slice(self: *const Self) []const T {
                    return self.v[0..dim];
                }

                // TODO: online works because inlined, dangling pointer.
                pub inline fn layout(
                    comptime shader_location: u32,
                    comptime step_mode: wgpu.VertexStepMode,
                ) wgpu.VertexBufferLayout {
                    const format: wgpu.VertexFormat = switch(dim) {
                        2 => .float32x2,
                        3 => .float32x3,
                        4 => .float32x4,
                        else => @compileError(
                            "Cannot create a Vector of type other than float or int."
                        ),
                    };
                    const attributes = [_]wgpu.VertexAttribute{
                        .{
                            .format = format,
                            .offset = 0,
                            .shader_location = shader_location,
                        },
                    };

                    return wgpu.VertexBufferLayout{
                        .array_stride = gpu_align,
                        .step_mode = step_mode,
                        .attribute_count = attributes.len,
                        .attributes = &attributes,
                    };
                }
            },
            .Int => struct {
                // Sending matrices of int to the gpu is not supported (yet).
            },
            else => @compileError(
                "Cannot create a Vector of type other than float or int."
            ),
        };


        pub usingnamespace switch(dim) {
            4 => struct {
                pub inline fn new(_x: T, _y: T, _z: T, _w: T) Self {
                    return Self{.v = .{_x, _y, _z, _w}};
                }

                pub inline fn z(self: Self)      T { return self.v[2]; }

                pub inline fn zMut(self: *Self) *T { return &self.v[2]; }

                pub inline fn b(self: Self)      T { return self.v[2]; }

                pub inline fn bMut(self: *Self) *T { return &self.v[2]; }

                pub inline fn w(self: Self)      T { return self.v[3]; }

                pub inline fn wMut(self: *Self) *T { return &self.v[3]; }

                pub inline fn a(self: Self)      T { return self.v[3]; }

                pub inline fn aMut(self: *Self) *T { return &self.v[3]; }

                pub inline fn fromVec3(vec: Vec3, _w: T) Self {
                    return Self.new(vec.x(), vec.y(), vec.z(), _w);
                }

                const components = enum { x, y, z, w };

                pub inline fn swizzle(
                    self: Self,
                    c0: components,
                    c1: components,
                    c2: components,
                    c3: components,
                ) Self {
                    return Self{.v = .{
                        self.v[@intFromEnum(c0)],
                        self.v[@intFromEnum(c1)],
                        self.v[@intFromEnum(c2)],
                        self.v[@intFromEnum(c3)],
                    }};
                }
            },
            3 => struct {
                pub inline fn new(_x: T, _y: T, _z: T) Self {
                    return Self{.v = .{_x, _y, _z}};
                }

                pub inline fn z(self: Self)      T { return self.v[2]; }

                pub inline fn zMut(self: *Self) *T { return &self.v[2]; }

                pub inline fn b(self: Self)      T { return self.v[2]; }

                pub inline fn bMut(self: *Self) *T { return &self.v[2]; }

                pub inline fn fromVec4(vec: Vec4) Self {
                    return Self.new(vec.x(), vec.y(), vec.z());
                }

                const components = enum { x, y, z };

                pub inline fn swizzle(
                    self: Self,
                    c0: components,
                    c1: components,
                    c2: components,
                ) Self {
                    return Self{.v = .{
                        self.v[@intFromEnum(c0)],
                        self.v[@intFromEnum(c1)],
                        self.v[@intFromEnum(c2)],
                    }};
                }

                pub inline fn cross(lhs: Self, rhs: Self) Self {
                    const l_yzx = lhs.swizzle(.y, .z, .x);
                    const r_yzx = rhs.swizzle(.y, .z, .x);
                    const tmp = lhs.mulSub(r_yzx, rhs.mulPerElem(l_yzx));
                    return tmp.swizzle(.y, .z, .x);
                }
            },
            2 => struct {
                pub inline fn new(_x: T, _y: T) Self {
                    return Self{.v = Simd{_x, _y}};
                }

                const components = enum { x, y };

                pub inline fn swizzle(
                    self: Self,
                    c0: components,
                    c1: components,
                ) Self {
                    return Self{.v = .{
                        self.v[@intFromEnum(c0)],
                        self.v[@intFromEnum(c1)],
                    }};
                }
            },
            else => @compileError(
                "Cannot create a Vector of dimension other than 2, 3 or 4."
            ),
        };

        pub inline fn fromArray (vec: V)     Self { return Self{.v = vec}; }
        pub inline fn fromVector(vec: Simd)  Self { return Self{.v = vec}; }

        pub inline fn splat(val: T) Self {
            return Self{.v = @as(Simd, @splat(val))};
        }
        pub inline fn zero() Self { return splat(zero_val); }
        pub inline fn one()  Self { return splat(one_val); }

        pub inline fn x   (self: Self)   T { return  self.v[0]; }
        pub inline fn xMut(self: *Self) *T { return &self.v[0]; }
        pub inline fn r   (self: Self)   T { return  self.v[0]; }
        pub inline fn rMut(self: *Self) *T { return &self.v[0]; }
        pub inline fn y   (self: Self)   T { return  self.v[1]; }
        pub inline fn yMut(self: *Self) *T { return &self.v[1]; }
        pub inline fn g   (self: Self)   T { return  self.v[1]; }
        pub inline fn gMut(self: *Self) *T { return &self.v[1]; }

        pub inline fn get(self: Self, idx: usize) T { return self.v[idx]; }
        pub inline fn set(self: *Self, idx: usize, val: T) void {
            self.v[idx] = val;
        }

        pub inline fn add(lhs: Self, rhs: Self) Self {
            return Self{.v = @as(Simd, lhs.v) + @as(Simd, rhs.v)};
        }

        pub inline fn addNeg(lhs: Self, rhs: Self) Self {
            return Self{.v = @as(Simd, lhs.v) + @as(Simd, rhs.neg().v)};
        }

        pub inline fn mulPerElem(lhs: Self, rhs: Self) Self {
            return Self{.v = @as(Simd, lhs.v) * @as(Simd, rhs.v)};
        }

        pub inline fn mulScalar(self: Self, scalar: T) Self {
            return Self{.v = @as(Simd, @splat(scalar)) * @as(Simd, self.v)};
        }

        pub inline fn neg(self: Self) Self { return self.mulScalar(-one_val); }

        pub inline fn mulAdd(m1: Self, m2: Self, a: Self) Self {
            return Self{.v = @mulAdd(Simd, m1.v, m2.v, a.v)};
        }

        pub inline fn mulSub(m1: Self, m2: Self, s: Self) Self {
            return Self{.v = @mulAdd(Simd, m1.v, m2.v, s.neg().v)};
        }

        pub fn mulMat(lhs: Self, rhs: Mat(dim, T)) Self {
            var res: Self = undefined;
            inline for (0..dim) |i| {
                res.set(i, lhs.dot(rhs.getCol(i)));
            }
            return res;
        }

        pub inline fn dot(lhs: Self, rhs: Self) T {
            return @reduce(.Add, @as(Simd, lhs.v) * @as(Simd, rhs.v));
        }

        pub inline fn norm2(self: Self) T { return dot(self, self); }
        pub inline fn norm (self: Self) T { return @sqrt(self.norm2()); }

        pub inline fn eql(lhs: Self, rhs: Self) bool {
            return @reduce(.And, (@as(Simd, lhs.v) == @as(Simd, rhs.v)));
        }
    };
}

pub fn Mat(dim: usize, comptime T: type) type {
    return extern struct {
        const V = Vec(dim, T);
        const M = [dim]V;
        const Simd = @Vector(dim, T);
        const Axis = enum(u2) { X, Y, Z };
        const Self = @This();

        m: M,

        const zero_val: T = switch(@typeInfo(T)) {
            .Float => 0.0,
            .Int => 0,
            else => @compileError(
                "Cannot create a Vector of type other than float or int."
            ),
        };
        const one_val: T = switch(@typeInfo(T)) {
            .Float => 1.0,
            .Int => 1,
            else => @compileError(
                "Cannot create a Vector of type other than float or int."
            ),
        };

        pub usingnamespace switch(dim) {
            4 => struct {
                pub inline fn fromCol(col0: V, col1: V, col2: V, col3: V) Self {
                    return Self{.m = .{col0, col1, col2, col3}};
                }

                pub inline fn translation(tx: T, ty: T, tz: T) Self {
                    var res = Self.identity();
                    res.set(0, 3, tx);
                    res.set(1, 3, ty);
                    res.set(2, 3, tz);
                    return res;
                }

                pub inline fn translationFromVec(trans: Vec3) Self {
                    var res = Self.identity();
                    res.setCol(3, Vec4.fromVec3(trans));
                    return res;
                }

                pub inline fn fromQuat(q: Quat) Self {
                    var res = Self.zero();
                    res.set(
                        0, 0,
                        2.0 * (q.a() * q.a() + q.b() * q.b()) - 1.0
                    );
                    res.set(
                        0, 1,
                        2.0 * (q.b() * q.c() - q.a() * q.d())
                    );
                    res.set(
                        0, 2,
                        2.0 * (q.b() * q.d() + q.a() * q.c())
                    );
                    res.set(
                        1, 0,
                        2.0 * (q.b() * q.c() + q.a() * q.d())
                    );
                    res.set(
                        1, 1,
                        2.0 * (q.a() * q.a() + q.c() * q.c()) - 1.0
                    );
                    res.set(
                        1, 2,
                        2.0 * (q.c() * q.d() - q.a() * q.b())
                    );
                    res.set(
                        2, 0,
                        2.0 * (q.b() * q.d() - q.a() * q.c())
                    );
                    res.set(
                        2, 1,
                        2.0 * (q.c() * q.d() + q.a() * q.b())
                    );
                    res.set(
                        2, 2,
                        2.0 * (q.a() * q.a() + q.d() * q.d()) - 1.0
                    );
                    res.set(3, 3, 1.0);

                    return res;
                }

                pub inline fn fromTransQuat(t: Vec3, q: Quat) Self {
                    var res = fromQuat(q);
                    res.setCol(3, Vec4.fromVec3(t, 1.0));
                    return res;
                }

                pub inline fn scale(sx: T, sy: T, sz: T) Self {
                    var res = Self.zero();
                    res.set(0, 0, sx);
                    res.set(1, 1, sy);
                    res.set(2, 2, sz);
                    res.set(3, 3, one_val);
                    return res;
                }

                pub inline fn scaleFromVec(vec: Vec3) Self {
                    var res = Self.zero();
                    res.set(0, 0, vec.x());
                    res.set(1, 1, vec.y());
                    res.set(2, 2, vec.z());
                    res.set(3, 3, one_val);
                    return res;
                }

                pub usingnamespace switch(@typeInfo(T)) {
                    .Float => struct {
                        pub inline fn rotationCosSin(
                            c: T,
                            s: T,
                            axis: Axis,
                        ) Self {
                            const axis_1 = (@intFromEnum(axis) + 1) % 3;
                            const axis_2 = (axis_1 + 1) % 3;
                            var res = Self.identity();
                            res.set(axis_1, axis_1, c);
                            res.set(axis_1, axis_2, -s);
                            res.set(axis_2, axis_1, s);
                            res.set(axis_2, axis_2, c);
                            return res;
                        }

                        pub inline fn rotation(angle: T, axis: Axis) Self {
                            const c = @cos(angle);
                            const s = @sin(angle);
                            return rotationCosSin(c, s, axis);
                        }
                    },
                    else => struct {},
                };

                pub inline fn mul(lhs: Self, rhs: Self) Self {
                    return Self.fromCol(
                        lhs.mulVec(rhs.getCol(0)),
                        lhs.mulVec(rhs.getCol(1)),
                        lhs.mulVec(rhs.getCol(2)),
                        lhs.mulVec(rhs.getCol(3)),
                    );
                }

                // User must ensure that this is a transform matrix, e.g. that
                // the last row is null expect for the last value being one.
                pub inline fn inverseTransform(self: Self) Self {
                    var res = Self.zero();
                    const col0_normalised2 = self.getCol(0)
                        .mulScalar(1.0 / self.getCol(0).norm2());
                    const col1_normalised2 = self.getCol(1)
                        .mulScalar(1.0 / self.getCol(1).norm2());
                    const col2_normalised2 = self.getCol(2)
                        .mulScalar(1.0 / self.getCol(2).norm2());
                    res.setRow(0, col0_normalised2);
                    res.setRow(1, col1_normalised2);
                    res.setRow(2, col2_normalised2);
                    res.set(0, 3, - self.getCol(3).dot(col0_normalised2));
                    res.set(1, 3, - self.getCol(3).dot(col1_normalised2));
                    res.set(2, 3, - self.getCol(3).dot(col2_normalised2));
                    res.set(3, 3, 1.0);
                    return res;
                }

                pub inline fn slice(self: *const Self) []const T {
                    return mem.bytesAsSlice(T, mem.asBytes(&self.m));
                }

                // TODO: online works because inlined, dangling pointer.
                pub inline fn layout(
                    comptime start_shader_location: u32,
                    comptime step_mode: wgpu.VertexStepMode,
                ) wgpu.VertexBufferLayout {
                    var attributes: [dim]wgpu.VertexAttribute = undefined;
                    inline for (0..dim) |i| {
                        attributes[i] = .{
                            .format = .float32x4,
                            .offset = i * V.gpu_align,
                            .shader_location = start_shader_location + i,
                        };
                    }

                    return wgpu.VertexBufferLayout{
                        .array_stride = @sizeOf(Self),
                        .step_mode = step_mode,
                        .attribute_count = attributes.len,
                        .attributes = &attributes,
                    };
                }
            },
            3 => struct {
                pub inline fn fromCol(col0: V, col1: V, col2: V) Self {
                    return Self{.m = .{col0, col1, col2}};
                }

                pub inline fn diag(sx: T, sy: T, sz: T) Self {
                    var res = Self.zero();
                    res.set(0, 0, sx);
                    res.set(1, 1, sy);
                    res.set(2, 2, sz);
                    return res;
                }

                pub inline fn diagFromVec(vec: V) Self {
                    var res = Self.zero();
                    res.set(0, 0, vec.x());
                    res.set(1, 1, vec.y());
                    res.set(2, 2, vec.z());
                    return res;
                }

                pub usingnamespace switch(@typeInfo(T)) {
                    .Float => struct {
                        pub inline fn rotationCosSin(c: T, s: T, axis: Axis) Self {
                            const axis_1 = (@intFromEnum(axis) + 1) % 3;
                            const axis_2 = (axis_1 + 1) % 3;
                            var res = Self.identity();
                            res.set(axis_1, axis_1, c);
                            res.set(axis_1, axis_2, -s);
                            res.set(axis_2, axis_1, s);
                            res.set(axis_2, axis_2, c);
                            return res;
                        }

                        pub inline fn rotation(angle: T, axis: Axis) Self {
                            const c = @cos(angle);
                            const s = @sin(angle);
                            return rotationCosSin(c, s, axis);
                        }
                    },
                    else => struct {},
                };

                pub inline fn mul(lhs: Self, rhs: Self) Self {
                    return Self.fromCol(
                        lhs.mulVec(rhs.getCol(0)),
                        lhs.mulVec(rhs.getCol(1)),
                        lhs.mulVec(rhs.getCol(2)),
                    );
                }

                // Matrices of dim 3 will not have a proper memory layout to be
                // sent to the gpu, so slice() and layout() do not exist.
            },
            2 => struct {
                pub inline fn fromCol(col0: V, col1: V) Self {
                    return Self{.m = .{col0, col1}};
                }

                pub inline fn diag(sx: T, sy: T) Self {
                    var res = Self.zero();
                    res.set(0, 0, sx);
                    res.set(1, 1, sy);
                    return res;
                }

                pub inline fn diagFromVec(vec: V) Self {
                    var res = Self.zero();
                    res.set(0, 0, vec.x());
                    res.set(1, 1, vec.y());
                    return res;
                }

                pub inline fn mul(lhs: Self, rhs: Self) Self {
                    return Self.fromCol(
                        lhs.mulVec(rhs.getCol(0)),
                        lhs.mulVec(rhs.getCol(1)),
                    );
                }

                pub inline fn slice(self: *const Self) []const T {
                    return mem.bytesAsSlice(T, mem.asBytes(&self.m));
                }

                // TODO: online works because inlined, dangling pointer.
                pub inline fn layout(
                    comptime start_shader_location: u32,
                    comptime step_mode: wgpu.VertexStepMode,
                ) wgpu.VertexBufferLayout {
                    var attributes: [dim]wgpu.VertexAttribute = undefined;
                    inline for (0..dim) |i| {
                        attributes[i] = .{
                            .format = .float32x2,
                            .offset = i * V.gpu_align,
                            .shader_location = start_shader_location + i,
                        };
                    }

                    return wgpu.VertexBufferLayout{
                        .array_stride = @sizeOf(Self),
                        .step_mode = step_mode,
                        .attribute_count = attributes.len,
                        .attributes = &attributes,
                    };
                }
            },
            else => @compileError(
                "Cannot create a Matrix of dimension other than 2, 3 or 4."
            ),
        };

        pub inline fn zero() Self {
            var m: M = undefined;
            inline for (0..dim) |i| {
                m[i] = V.zero();
            }
            return Self{.m = m};
        }

        pub inline fn one() Self {
            var m: M = undefined;
            inline for (0..dim) |i| {
                m[i] = V.one();
            }
            return Self{.m = m};
        }

        pub inline fn identity() Self {
            var res = zero();
            inline for (0..dim) |i| {
                res.set(i, i, one_val);
            }
            return res;
        }

        pub inline fn get(self: Self, row: usize, col: usize) T {
            return self.m[col].get(row);
        }

        pub inline fn getCol(self: Self, col: usize) V {
            return self.m[col];
        }

        pub inline fn getRow(self: Self, row: usize) V {
            var res: V = undefined;
            inline for (0..dim) |i| {
                res.set(i, self.get(row, i));
            }
            return res;
        }

        pub inline fn set(self: *Self, row: usize, col: usize, val: T) void {
            self.m[col].set(row, val);
        }

        pub inline fn setCol(self: *Self, col: usize, val: V) void {
            self.m[col] = val;
        }

        pub inline fn setRow(self: *Self, row: usize, val: V) void {
            inline for (0..dim) |i| {
                self.set(row, i, val.get(i));
            }
        }

        pub inline fn mulVec(self: Self, vec: V) V {
            var res = V.zero();
            inline for (0..dim) |i| {
                res = V.mulAdd(
                    V.splat(vec.v[i]),
                    self.m[i],
                    res,
                );
            }
            return res;
        }

        pub inline fn transpose(self: Self) Self {
            var res: Self = undefined;
            for (0..dim) |i| {
                res.setRow(i, self.getCol(i));
            }
            return res;
        }
    };
}

pub const Quat = struct {
    const Self = @This();

    re: f32,
    im: Vec3,

    pub inline fn new(_angle: f32, dir: Vec3) Self {
        return newNormalised(_angle, dir.normalise());
    }

    pub inline fn newNormalised(_angle: f32, dir: Vec3) Self {
        return Self{
            .re = @cos(_angle / 2.0),
            .im = dir.mulScalar(@sin(_angle / 2.0)),
        };
    }

    pub inline fn pure(vec: Vec3) Self {
        return Self{
            .re = 0.0,
            .im = vec,
        };
    }

    pub inline fn zero() Self {
        return Self{
            .re = 1.0,
            .im = Vec3.zero(),
        };
    }

    pub inline fn a(self: Self) f32 {
        return self.re;
    }

    pub inline fn b(self: Self) f32 {
        return self.im.x();
    }

    pub inline fn c(self: Self) f32 {
        return self.im.y();
    }

    pub inline fn d(self: Self) f32 {
        return self.im.z();
    }

    pub inline fn angle(self: Self) f32 {
        return 2.0 * std.math.acos(self.re);
    }

    pub inline fn direction(self: Self) Vec3 {
        return self.im.normalise();
    }

    pub inline fn conjugate(self: Self) Self {
        return Self{
            .re = self.re,
            .im = self.im.neg(),
        };
    }

    pub inline fn mul(lhs: Self, rhs: Self) Self {
        return Self{
            .re = (lhs.re * rhs.re) - lhs.im.dot(rhs.im),
            .im = rhs.im.mulScalar(lhs.re)
                .add(lhs.im.mulScalar(rhs.re))
                .add(lhs.im.cross(rhs.im)),
        };
    }

    pub inline fn compose(first: Self, second: Self) Self {
        return first.mul(second);
    }

    pub inline fn apply(self: Self, vec: Vec3) Vec3 {
        return (self.mul(pure(vec)).mul(self.conjugate()).im);
    }
};
