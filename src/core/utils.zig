const std = @import("std");


pub fn baseType(comptime T: type) type {
    return switch (@typeInfo(T)) {
        .Array => |info| info.child,
        .Vector => |info| info.child,
        .Pointer => |info| info.child,
        .Optional => |info| info.child,
        else => T,
    };
}

pub fn orphanTypeName(name: [:0]const u8) [:0]const u8 {
    var start = name.len - 2;
    while (start > 0) {
        if (name[start] == '.') {
            return name[(start + 1)..];
        }
        start -= 1;
    }
    return name;
}
