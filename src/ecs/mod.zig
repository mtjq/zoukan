pub const World = @import("world.zig").World;
pub const Query = @import("query.zig").Query;


test "ecs" {
    _ = @import("world.zig");
    _ = @import("component.zig");
    _ = @import("query.zig");
}
