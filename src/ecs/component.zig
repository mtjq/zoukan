const std = @import("std");
const HashMap = std.hash_map.AutoHashMap;

const EntityId = @import("world.zig").EntityId;


pub const ComponentInstances = struct {
    allocator: std.mem.Allocator,
    ent_to_comp: HashMap(EntityId, usize),
    size: usize,
    alignment: usize,
    data: []u8,
    len: usize,
    capacity: usize,

    const Self = @This();

    pub fn deinit(self: *Self) void {
        self.allocator.free(self.data);
        self.ent_to_comp.deinit();
    }

    pub fn set(self: *Self, ent: EntityId, c: anytype) !void {
        if (self.ent_to_comp.get(ent)) |idx| {
            std.mem.copyForwards(
                u8,
                self.data[(idx * self.size)..],
                std.mem.sliceAsBytes((&c)[0..1])
            );
        } else {
            if (self.len == self.capacity) {
                self.capacity = (self.capacity + 1) * 2;

                const data_tmp = try self.allocator.alloc(
                    u8,
                    self.capacity * self.size
                );
                std.mem.copyForwards(u8, data_tmp, self.data);
                self.allocator.free(self.data);
                self.data = data_tmp;
            }

            std.mem.copyForwards(
                u8,
                self.data[(self.len * self.size)..],
                std.mem.sliceAsBytes((&c)[0..1])
            );
            try self.ent_to_comp.put(ent, self.len);
        }

        self.len += 1;
    }

    pub fn getComponent(self: Self, ent: EntityId) ?[]u8 {
        const idx = self.ent_to_comp.get(ent) orelse return null;

        return self.data[(idx * self.size)..((idx + 1) * self.size)];
    }

    pub fn getEntities(self: Self) self.ent_to_comp.KeyIterator {
        return self.ent_to_comp.keyIterator();
    }

    pub fn remove(self: *Self, ent: EntityId) void {
        const idx = self.ent_to_comp.get(ent) orelse return;

        std.mem.copyForwards(
            u8,
            self.data[(idx * self.size)..],
            self.data[((idx + 1) * self.size)..(self.len * self.size)]
        );
        self.len -= 1;
        _ = self.ent_to_comp.remove(ent);
    }
};


test "Component" {
    const allocator = std.testing.allocator;

    const data = try allocator.alloc(u8, 1 * @sizeOf(u64));
    var components = ComponentInstances{
        .allocator = allocator,
        .ent_to_comp = HashMap(EntityId, usize).init(allocator),
        .size = @sizeOf(u64),
        .alignment = @alignOf(u64),
        .data = data,
        .len = 0,
        .capacity = 1,
    };
    defer components.deinit();

    try components.set(0, @as(u64, 0));

    try components.set(1, @as(u64, 0x7a6967));
    try std.testing.expect(
        std.mem.bytesAsValue(
            u64,
            @as(
                []align(@alignOf(u64)) const u8,
                @alignCast(components.getComponent(1).?)
            )
        ).* == 0x7a6967
    );

    try components.set(0, @as(u64, 0));
    components.remove(0);
    try std.testing.expect(components.getComponent(0) == null);

    try components.set(1, @as(u64, 0x7a6f756b616e));
    try std.testing.expect(
        std.mem.bytesAsValue(
            u64,
            @as(
                []align(@alignOf(u64)) const u8,
                @alignCast(components.getComponent(1).?)
            )
        ).* == 0x7a6f756b616e
    );
}
