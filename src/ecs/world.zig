const std = @import("std");
const HashMap = std.hash_map.AutoHashMap;
const StringHashMap = std.hash_map.StringHashMap;

const comp = @import("component.zig");
const Query = @import("query.zig").Query;
const baseType = @import("core").utils.baseType;
const orphanTypeName = @import("core").utils.orphanTypeName;


pub const EntityId = usize;

pub fn World(comptime all_components: []const type) type {
    return struct {
        allocator: std.mem.Allocator,
        components: StringHashMap(comp.ComponentInstances),
        entity_count: usize,

        const Self = @This();

        pub fn init(allocator: std.mem.Allocator) !Self {
            const capacity = 8;
            var components =
                StringHashMap(comp.ComponentInstances).init(allocator);
            inline for (all_components) |C| {
                const data = try allocator.alloc(u8, capacity * @sizeOf(C));
                try components.put(
                    @typeName(C),
                    comp.ComponentInstances{
                        .allocator = allocator,
                        .ent_to_comp = HashMap(EntityId, usize).init(allocator),
                        .size = @sizeOf(C),
                        .alignment = @alignOf(C),
                        .data = data,
                        .len = 0,
                        .capacity = capacity,
                    }
                );
            }

            return Self{
                .allocator = allocator,
                .components = components,
                .entity_count = 0,
            };
        }

        pub fn deinit(self: *Self) void {
            var it = self.components.valueIterator();
            while (it.next()) |cs| {
                cs.deinit();
            }
            self.components.deinit();
        }

        pub fn newEntity(self: *Self) !EntityId {
            self.entity_count += 1;
            return self.entity_count - 1;
        }

        pub fn setComponent(self: *Self, ent: EntityId, c: anytype) !void {
            const cs = self.components.getPtr(@typeName(@TypeOf(c))).?;
            try cs.set(ent, c);
        }

        pub fn getComponentPtr(self: Self, ent: EntityId, C: type) ?*C {
            const cs = self.components.get(@typeName(baseType(C))).?;
            const bytes = cs.getComponent(ent) orelse return null;
            const alignment = @alignOf(C);
            return std.mem.bytesAsValue(
                C,
                @as([]align(alignment) u8,
                    @alignCast(bytes))
            );
        }

        pub fn getComponent(self: Self, ent: EntityId, C: type) ?C {
            if (self.getComponentPtr(ent, C)) |res| {
                return res.*;
            } else {
                return null;
            }
        }

        pub fn removeComponent(self: *Self, ent: EntityId, C:type) void {
            const cs = self.components.getPtr(@typeName(C)).?;
            cs.remove(ent);
        }

        pub fn query(
            self: *Self,
            comptime components: []const type,
        ) !?Query(components) {
            if (components.len == 0) {
                return null;
            }

            var entities = std.ArrayList(EntityId).init(self.allocator);
            defer entities.deinit();
            var key_iter_0 = self.components
                .getPtr(@typeName(baseType(components[0]))).?
                .ent_to_comp
                .keyIterator();
            while (key_iter_0.next()) |ent| {
                try entities.append(ent.*);
            }
            inline for (1..components.len) |i| {
                var j: usize = 0;
                foo: while (j < entities.items.len) {
                    var key_iter = self.components
                        .getPtr(@typeName(baseType(components[i]))).?
                        .ent_to_comp
                        .keyIterator();
                    while (key_iter.next()) |ent| {
                        if (entities.items[j] == ent.*) {
                            j += 1;
                            continue: foo;
                        }
                    }
                    _ = entities.orderedRemove(j);
                }
            }
            if (entities.items.len == 0) {
                return null;
            }

            var res = Query(components).init(self.allocator);
            for (entities.items) |ent| {
                var item: Query(components).Item = undefined;
                inline for (components) |C| {
                    const base_type = baseType(C);
                    @field(item, orphanTypeName(@typeName(base_type))) =
                        switch (@typeInfo(C)) {
                            .Pointer => self.getComponentPtr(ent, base_type).?,
                            else => self.getComponent(ent, C).?,
                        };
                }
                try res.append(item);
            }
            return res;
        }
    };
}


test "World building" {
    const Position = struct {
        x: f32,
        y: f32,
        z: f32,
    };

    const Tag = struct {
        tag: []const u8,
    };

    const all_components = [_]type{
        Position,
        Tag,
    };

    var world = try World(&all_components).init(std.testing.allocator);
    defer world.deinit();

    const ent1 = try world.newEntity();
    try world.setComponent(ent1, Position{.x = 1.0, .y = 0.0, .z = 0.0});
    try world.setComponent(ent1, Tag{.tag = "Zoukan"});

    const ent2 = try world.newEntity();
    const pos2 = Position{.x = 0.0, .y = 1.0, .z = 0.0};
    try world.setComponent(ent2, pos2);

    try std.testing.expect(world.entity_count == 2);
    try std.testing.expect(
        std.mem.eql(
            u8,
            world.getComponent(ent1, Tag).?.tag,
            "Zoukan"
        )
    );
    try std.testing.expect(world.getComponent(ent2, Position).?.y == 1.0);

    world.removeComponent(ent1, Tag);
    try std.testing.expect(world.getComponent(ent1, Tag) == null);
}

test "World query" {
    const all_components = [_]type{
        i32,
        f32,
    };

    var world = try World(&all_components).init(std.testing.allocator);
    defer world.deinit();

    const ent1 = try world.newEntity();
    try world.setComponent(ent1, @as(i32, 53));
    try world.setComponent(ent1, @as(f32, 2.7));

    const ent2 = try world.newEntity();
    try world.setComponent(ent2, @as(i32, 72));

    var query_all = (try world.query(&all_components)).?;
    defer query_all.deinit();

    try std.testing.expect(query_all.next().?.f32 == 2.7);
    try std.testing.expect(query_all.next() == null);

    var query_i32 = (try world.query(&.{*i32})).?;
    defer query_i32.deinit();

    query_i32.next().?.i32.* = 35;
    try std.testing.expect(query_i32.next().?.i32.* == 72);
    try std.testing.expect(query_i32.next() == null);
    try std.testing.expect(world.getComponent(ent1, i32) == 35);
}
