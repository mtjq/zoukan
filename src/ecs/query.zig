const std = @import("std");

const baseType = @import("core").utils.baseType;
const orphanTypeName = @import("core").utils.orphanTypeName;


pub fn Query(comptime components: []const type) type {
    return struct {
        items: std.ArrayList(Item),
        idx: usize,

        const Self = @This();

        pub const Item = T: {
            var fields: [components.len]std.builtin.Type.StructField
                = undefined;
            for (components, 0..) |C, i| {
                fields[i] = .{
                    .name = orphanTypeName(@typeName(baseType(C))),
                    .type = C,
                    .default_value = null,
                    .is_comptime = false,
                    .alignment = @alignOf(C),
                };
            }
            break :T @Type(.{.Struct = .{
                .layout = .auto,
                .backing_integer = null,
                .fields = &fields,
                .decls = &.{},
                .is_tuple = false,
            }});
        };

        pub fn init(allocator: std.mem.Allocator) Self {
            return Self {
                .items = std.ArrayList(Item).init(allocator),
                .idx = 0,
            };
        }

        pub fn deinit(self: *Self) void {
            self.items.deinit();
        }

        pub fn append(self: *Self, item: Item) !void {
            try self.items.append(item);
        }

        pub fn next(self: *Self) ?Item {
            if (self.idx == self.items.items.len) {
                return null;
            } else {
                self.idx += 1;
                return self.items.items[self.idx - 1];
            }
        }

        pub fn reset(self: *Self) void {
            self.idx = 0;
        }

        pub fn size(self: Self) usize {
            return self.items.items.len;
        }
    };
}


test "Query" {
    var query = Query(&.{*i32, f32}).init(std.testing.allocator);
    defer query.deinit();

    var int_var: i32 = 53;
    try query.append(.{.i32 = &int_var, .f32 = 2.7});

    var q = query.next().?;
    q.i32.* = 35;
    try std.testing.expect(int_var == 35);
    try std.testing.expect(q.f32 == 2.7);
    try std.testing.expect(query.next() == null);

    query.reset();
    q = query.next().?;
    q.i32.* = 53;
    try std.testing.expect(int_var == 35); // Should not work.
    try std.testing.expect(q.f32 == 2.7);
    try std.testing.expect(query.next() == null);
}
