const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    // Mach glfw/gpu.
    const glfw_dep = b.dependency("mach_glfw", .{
        .target = target,
        .optimize = optimize,
    });
    const gpu_dep = b.dependency("mach_gpu", .{
        .target = target,
        .optimize = optimize,
    });

    // Modules.
    const core_mod = b.createModule(.{
        .root_source_file = b.path("src/core/mod.zig"),
        .target = target,
        .optimize = optimize,
    });
    core_mod.addImport("mach-glfw", glfw_dep.module("mach-glfw"));
    core_mod.addImport("mach-gpu", gpu_dep.module("mach-gpu"));
    const ecs_mod = b.addModule("ecs", .{
        .root_source_file = b.path("src/ecs/mod.zig"),
        .target = target,
        .optimize = optimize,
    });
    ecs_mod.addImport("core", core_mod);
    const render_mod = b.addModule("render", .{
        .root_source_file = b.path("src/render/mod.zig"),
        .target = target,
        .optimize = optimize,
    });
    core_mod.addImport("render", render_mod);
    render_mod.addImport("mach-glfw", glfw_dep.module("mach-glfw"));
    render_mod.addImport("mach-gpu", gpu_dep.module("mach-gpu"));
    render_mod.addImport("core", core_mod);
    render_mod.addImport("ecs", ecs_mod);
    const NamedModule = struct {
        name: []const u8,
        mod: *std.Build.Module,
    };
    const modules = [_]NamedModule{
        .{.name = "core", .mod = core_mod},
        .{.name = "ecs", .mod = ecs_mod},
        .{.name = "render", .mod = render_mod},
    };

    // Lib.
    const lib = b.addStaticLibrary(.{
        .name = "zoukan",
        .root_source_file = b.path("src/zoukan.zig"),
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(lib);
    try @import("mach_gpu").link(gpu_dep.builder, lib, &lib.root_module, .{});
    for (modules) |mod| {
        lib.root_module.addImport(mod.name, mod.mod);
    }

    // Exe.
    const exe = b.addExecutable(.{
        .name = "app",
        .root_source_file = b.path("src/app.zig"),
        .target = target,
        .optimize = optimize,
    });
    exe.root_module.addImport("zoukan", &lib.root_module);

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    // Lib tests.
    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/zoukan.zig"),
        .target = target,
        .optimize = optimize,
    });
    for (modules) |mod| {
        lib_unit_tests.root_module.addImport(mod.name, mod.mod);
    }
    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);

    // Exe tests.
    const exe_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/app.zig"),
        .target = target,
        .optimize = optimize,
    });
    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);
    test_step.dependOn(&run_exe_unit_tests.step);
}
